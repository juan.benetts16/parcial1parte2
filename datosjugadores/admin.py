from django.contrib import admin

from .models import jugadoresNFLP, equiposNFLP, estadioNFLP 

# Register your models here.

@admin.register(jugadoresNFLP)
class AdminJugadores(admin.ModelAdmin):
	list_display = [
		"NombreJugador",	
		"Equipo",
		"Peso",
		"Altura",
		"Numero",
		"Posicion",
		"Despedido",
		"Intercambio",
		"Slug",
	]

@admin.register(equiposNFLP)
class AdminEquipos(admin.ModelAdmin):
	list_display = [
		"NombreEquipo",
		"Estadio",
		"Copas",
		"Slug",
	]

@admin.register(estadioNFLP)
class AdminEstadio(admin.ModelAdmin):
	list_display = [
		"NombreEstadio",
		"Capacidad",
		"Locacion",
		"Slug",
	]