from django.db import models
from django.conf import settings

# Create your models here.
from django.conf import settings

class estadioNFLP(models.Model):
	NombreEstadio = models.CharField(max_length = 50)
	Capacidad = models.IntegerField()
	Locacion = models.CharField(max_length = 50)
	Slug = models.SlugField()

	def __str__(self):
		return self.NombreEstadio

class equiposNFLP(models.Model):
	NombreEquipo = models.CharField(max_length = 50)
	Estadio = models.ForeignKey(estadioNFLP, on_delete = models.CASCADE)
	Copas = models.IntegerField()
	Slug = models.SlugField()

	def __str__(self):
		return self.NombreEquipo

class jugadoresNFLP(models.Model):
	NombreJugador = models.CharField(max_length = 50)
	Equipo = models.ForeignKey(equiposNFLP, on_delete = models.CASCADE)
	Peso = models.IntegerField()
	Altura = models.IntegerField()
	Numero = models.IntegerField()
	Posicion = models.CharField(max_length = 50, choices = [("QB", "QB"), ("Receiver", "Receiver"), ("Defense", "Defense")])
	Despedido = models.BooleanField()
	Intercambio = models.BooleanField()
	Slug = models.SlugField()

	def __str__(self):
		return self.NombreJugador







