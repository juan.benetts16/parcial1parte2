from django.shortcuts import render

from .models import estadioNFLP, equiposNFLP, jugadoresNFLP
# Create your views here.

# RETRIEVE

def EstadioLista(request):
	queryset = estadioNFLP.objects.all()
	context = {
	"queryset" : queryset,
	}
	return render(request, "nflp/ListaEstadios.html", context)

def EquiposLista(request):
	queryset = equiposNFLP.objects.all()
	context = {
	"queryset" : queryset,
	}
	return render(request, "nflp/ListaEquipos.html", context)

def JugadoresLista(request):
	queryset = jugadoresNFLP.objects.all()
	context = {
	"queryset" : queryset,
	}
	return render(request, "nflp/ListaJugadores.html", context)

#Details

def DetallesEstadios(request, id):
	queryset = estadioNFLP.objects.get(id=id)
	context = {
	"queryset" : queryset,
	}
	return render(request, "nflp/detailsEstadios.html", context)

def DetallesEquipos(request, id):
	queryset = equiposNFLP.objects.get(id=id)
	context = {
	"queryset" : queryset,
	}
	return render(request, "nflp/detailsEquipos.html", context)

def DetallesJugadores(request, id):
	queryset = jugadoresNFLP.objects.get(id=id)
	context = {
	"queryset" : queryset,
	}
	return render(request, "nflp/detailsJugadores.html", context)

