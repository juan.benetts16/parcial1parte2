from django.urls import path
from datosjugadores import views

urlpatterns = [
	#Agregamos las rutas de las vistas que se quieren mostrar
	path('listaestadios/', views.EstadioLista, name="listaestadios"),
	path('listajugadores/', views.JugadoresLista, name="listaestadios"),
	path('listaequipos/', views.EquiposLista, name="listaestadios"),
	path('detallesquipos/<int:id>/', views.DetallesEquipos, name="detallesequipos"),
	path('detallesestadios/<int:id>/', views.DetallesEstadios, name="detallesestadios"),
	path('detallesjugadores/<int:id>/', views.DetallesJugadores, name="detallesjugadores"),
]